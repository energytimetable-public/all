ALTER SYSTEM SET
    max_connections = '2000';
ALTER SYSTEM SET
    shared_buffers = '4GB';
ALTER SYSTEM SET
    effective_cache_size = '12GB';
ALTER SYSTEM SET
    maintenance_work_mem = '1GB';
ALTER SYSTEM SET
    checkpoint_completion_target = '0.9';
ALTER SYSTEM SET
    wal_buffers = '16MB';
ALTER SYSTEM SET
    default_statistics_target = '100';
ALTER SYSTEM SET
    random_page_cost = '1.1';
ALTER SYSTEM SET
    effective_io_concurrency = '200';
ALTER SYSTEM SET
    work_mem = '1048kB';
ALTER SYSTEM SET
    min_wal_size = '2GB';
ALTER SYSTEM SET
    max_wal_size = '8GB';
ALTER SYSTEM SET
    max_worker_processes = '4';
ALTER SYSTEM SET
    max_parallel_workers_per_gather = '2';
ALTER SYSTEM SET
    max_parallel_workers = '4';
ALTER SYSTEM SET
    max_parallel_maintenance_workers = '2';

create type distributor_type as enum ('transformator', 'distribution_cabine');

alter type distributor_type owner to postgres;

create table if not exists plans
(
    id                uuid      not null,
    plan_date         timestamp not null,
    created_at        timestamp,
    updated_at        timestamp,
    deleted_at        timestamp,
    original_data     jsonb,
    modified_data     jsonb,
    total             double precision,
    total_consumption double precision,
    total_production  double precision,
    constraint table_name_pk
        primary key (id, plan_date)
);

alter table plans
    owner to postgres;

create table if not exists distributions
(
    id                  uuid not null
        constraint transformers_pk
            primary key,
    created_at          timestamp,
    updated_at          timestamp,
    deleted_at          timestamp,
    parent_id           uuid
        constraint transformers_transformers_id_fk
            references distributions,
    input_connection_id uuid,
    distributor_type    distributor_type
);

alter table distributions
    owner to postgres;

create table distribution_data
(
    distributor_id   uuid not null
        constraint transformer_data_transformers_id_fk
            references distributions,
    meter_id         uuid not null,
    connection_id    uuid,
    push_destination text,
    constraint transformer_data_pk
        primary key (meter_id, distributor_id)
);

alter table distribution_data
    owner to postgres;

create unique index distribution_data_meter_id_uindex
    on distribution_data (meter_id);

INSERT INTO public.plans (id, plan_date, created_at, updated_at, deleted_at, original_data, modified_data)
VALUES ('e978a1a7-45aa-451a-84bd-ae6cdddeb528', '2006-01-25 00:00:00.000000', '2022-05-10 15:37:33.829871',
        '2022-05-23 15:30:29.790196', null, '{
    "id": "e978a1a7-45aa-451a-84bd-ae6cdddeb528",
    "date": "2006-01-25",
    "plans": [
      {
        "entries": [
          {
            "consumer_producers": [
              {
                "name": "test",
                "value": 1.5
              }
            ]
          }
        ],
        "plan_type": "Test"
      }
    ]
  }', '{
    "id": "e978a1a7-45aa-451a-84bd-ae6cdddeb528",
    "date": "2006-01-25",
    "plans": [
      {
        "entries": [
          {
            "quarter_of_day": 0,
            "consumer_producers": [
              {
                "name": "test",
                "value": 1.5
              }
            ]
          }
        ],
        "plan_type": "Test"
      }
    ]
  }');

create index plans_id_index
    on plans (id);

create index plans_plan_date_index
    on plans (plan_date);

CREATE OR REPLACE FUNCTION public.get_all_children_of_parent(use_parent uuid) RETURNS uuid[] AS
$BODY$
DECLARE
    process_parents uuid[] := ARRAY [ use_parent ];
    children        uuid[] := '{}';
    new_children    uuid[];
BEGIN
    WHILE (array_upper(process_parents, 1) IS NOT NULL)
        LOOP
            new_children := ARRAY(SELECT id
                                  FROM distributions
                                  WHERE parent_id = ANY (process_parents)
                                    AND id <> ALL (children));
            children := children || new_children;
            process_parents := new_children;
        END LOOP;
    RETURN children;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;